# Summary

## Links

In case it was missed:
-	[BitBucket URL](https://bitbucket.org/rf5860/bashful)
-	[Slides](https://gitpitch.com/rf5860/bashful/master?grs=bitbucket&t=black)

I'll send through my shell configuration files once I've checked it for any passwords.

## Quick-Start to Awesomeness 🌈

### Mac

Assumes you have [Homebrew](http://docs.brew.sh/Installation.html) installed

Once you have installed zsh, you might want to [set it as your default shell](http://osxdaily.com/2012/03/21/change-shell-mac-os-x/)

```bash
############################################
# Install pgcli, fpp, zsh with completions #
############################################

brew update
brew tap-pin dbcli/tap
brew install pgcli zsh zsh-completions fpp

#######################################
# Install iTerm2                      #
#######################################

brew cask install iterm2

##########################
# Install ZGEN + Plugins #
##########################

git clone https://github.com/tarjoilija/zgen.git "${HOME}/.zgen"
echo 'source "${HOME}/.zgen/zgen.zsh"' >> ~/.zshrc
echo '# if the init scipt doesn't exist
if ! zgen saved; then
  zgen load zdharma/fast-syntax-highlighting
  zgen save
fi' >> ~/.zshrc

###############
# Install FZF #
###############

brew install fzf
/usr/local/opt/fzf/install # Installs key bindings:
                           # Ctrl-T: Fuzzy file selection
                           # Ctrl-R: Fuzzy history selection
                           #  Alt-C: Change directory (Fuzzy-find from currrent)
```

### Windows

- [ConEmu](https://github.com/Maximus5/ConEmu) is a great terminal I have used in the past
- FZF binaries are available [here](https://github.com/junegunn/fzf-bin/releases)
- For pgcli:
  - Install [Python 3.4](https://docs.python.org/3/whatsnew/3.4.html) - it comes with pip
  - Run `pip install -U pgcli`
- You might have trouble getting zsh to work on windows. A good alternative is [Babun](http://babun.github.io/) - it includes oh-my-zsh and a heap of other cool stuff:

## Key Points

1. [Shortcuts](http://readline.kablamo.org/emacs.html) - because why go the long way
2. [fzf](https://github.com/junegunn/fzf) is outstanding - especially with history and [git](https://junegunn.kr/2015/03/browsing-git-commits-with-fzf/) (And [previews](https://junegunn.kr/2016/07/fzf-git/) too!)
3. [Git number](https://github.com/holygeek/git-number) is a time-saver
4. [Fuzzy Path Picker](https://facebook.github.io/PathPicker/) by Facebook is awesome when used with git
4. [iTerm2](https://www.iterm2.com/downloads.html) v3 is the bees knees
5. [ZSH](http://www.zsh.org/) is awesome - I recommend [zgen plugin manager](https://github.com/tarjoilija/zgen). Make [it pretty](https://github.com/zdharma/fast-syntax-highlighting)
6. [pgcli](https://www.pgcli.com/) - Bonus item; if you use psql, you should definitely migrate to pgcli, for autocomplete alone
7. Aliases are amazing! The commitWithJira one below is a personal favourite

```bash

function currentBranch() {
	git rev-parse --abbrev-ref HEAD
}

function currentJira() {
	currentBranch | grep -Po 'EN(AB)?-\d{1,4}'
}

function commitWithJira() {
	currentJira=$(currentJira 2>/dev/null && printf "- ")
	git commit -m "${currentJira}${@}"
}

# You can type "C "Some Message" - and "ENAB-1234 - " will automatically be insert at the start
# If there's no JIRA in the branch name, it won't be modified

alias C=commitWithJira
```
