# Bashful

A whirlwind tour of using the Command Line

---

## Beginning at the End

`Terminal`
> adjective
> 1. forming or situated at the end or extremity of something.

--

### Options

* Terminal (Stock)
* [Hyper](https://hyper.is/) - for the JS Inclined
* [iTerm2 - macOS Terminal Replacement](https://www.iterm2.com/)

---

### Terminal

Has gotten a lot better in recent releases

--

#### Advantages

* Common - available on basically all systems
* No external dependencies
* Performance
* Customization options

--

#### Disadvantages

* Lack of community

---

### iTerm2

The brand chosen by 9/10 cats

--

#### Features

* Hotkey Window Activation
* Better Search Capabilities
* Autocompletion
* Mouseless Copy
* Paste History
* Strong Community
* Configurability
* [...and more](https://www.iterm2.com/features.html)

---

### Hyper

Making a lot of noise lately. Electron based shell

--

#### Prominent Features

* Strong community
* Multi-platform
* Standard Extension Platform (npm)
* Beautiful Interface
* Nice API
* Powerful Extension Possibilities

![Powermode](https://cloud.githubusercontent.com/assets/13041/16820268/13c9bfe6-4905-11e6-8fe4-baf8fc8d9293.gif)

---

## Picking a Shell

Every turtle needs a home

--

### Common Choices

- Bash (Stock - 3.2.5) - Please, no..

  <section>
  <iframe width="420" height="345" src="https://www.youtube.com/embed/H07zYvkNYL8"></iframe>
  </section>

- [Bash 4.4.+](https://www.gnu.org/software/bash/)
- [ZSH](http://www.zsh.org/)

---

### Bash 4+

The stock shell is ancient (4.2 released in 2011)

Apart from bug fixes...

--

#### Notable New Features

(Details covered later)

* Associative Arrays
* Globbing (Details Later)
* Expansion (Parameter & Brace)

[More details here](http://wiki.bash-hackers.org/bash4).

---

### ZSH

The [new cool kid in town](https://www.slideshare.net/jaguardesignstudio/why-zsh-is-cooler-than-your-shell-16194692)

Has things like:

* Enhanced globbing
* Spelling correction (In auto-completions)
* Better history tracking
* Compatibility mode for other shells

Vibrant community, replete with Package Managers and Frameworks

--

#### Package Managers

* [Antigen](https://github.com/zsh-users/antigen)
* [ZGen](https://github.com/tarjoilija/zgen)
* [ZPlug](https://github.com/zplug/zplug)

--

#### Frameworks

* [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh) - The most popular
* [Prezto](https://github.com/sorin-ionescu/prezto) - Strong contender


---

## Using the Shell

Things common regardless of the programs you're planning on running.

---

### Key Bindings

Starting Simple - Time Travel

* Forward through history
* Backward through history

Sometimes though, the command you ran is quite a bit in the past, and it can be difficult to track it down. Fortunately, there's  shortcut to help us with that. In particular, Ctrl-r.

--

Find the last commit command we typed:

```
Ctrl-r
commit
Ctrl-a
```

Then we can edit the command as necessary. Nifty!

Fun fact: Emacs inherited a lot of these key-bindings

--

Shortcuts on the Line

| Keystroke | Action                                                  |
|:----------|:--------------------------------------------------------|
| Ctrl-[ae] | Go to the st[a]rt or [e]nd of the line                  |
| Ctrl-k    | Delete from the current position to the end of the line |
| Ctrl-y    | Paste deleted contents                                  |
| Alt-.     | Last word pf the previous command                       |
| Alt-t     | Transpose words                                         |
| Alt-[fb]  | Go [f]orward or [backward] a word                       |
| Alt-d     | [D]elete until the end of the word                      |
| Alt-[cul] | Change case [Capitalize &#124; Upper &#124; Lower]      |

--

Useful shortcuts for modifying the state of the terminal:

| Keystroke | Action           |
|:----------|:-----------------|
| Ctrl-l    | Clear the screen |

---

### Bash Profile

Runs on opening a new terminal. Good placed to save functions and aliases

Depending on the shell:

* ~/.zshrc
* ~/.bashrc

---

### Aliases

Cutting corners is always a good move

--

Aliases are shortcuts for frequently used commands.

Tired of typing `git add`?:
```bash
$ alias ga='git add'
$ ga "Some files"
```

--

#### Git Aliases

Some useful Git Aliases

```sh
function currentBranch() {
	git rev-parse --abbrev-ref HEAD
}

function currentJira() {
	currentBranch | grep -Po 'EN(AB)?-\d{1,4}'
}

function commitWithJira() {
	currentJira=$(currentJira 2> /dev/null && printf "- ")
	git commit -m "$currentJira$@"
}

alias C=commitWithJira
```

---

## History

Good info in the manual under [Event Designators](http://www.gnu.org/software/bash/manual/bashref.html#Event-Designators)

--

### Looking through Time

View history by running `history`:

```bash
$ history|tail -3
  565  history
  566  history|tail
  567  history|tail -3
```

--

### History Substitution

Rewriting history - always a good idea

* Simplest substitution - `replace in last command`.

Example:

```bash
$ git add file.txt
$ ^add^reset
git reset file.txt
```

--

### History Expansion

History starts from 0 (Kind of make sense...)

* Run the last command with `!!`.
* Run history item n with `!n`.
* Run the nth last command with `!-n`.
* Run the last command starting with a particular string by typing `!command` - where command can be replaced with anything.

```bash
echo "Hello"
cd ~
!ec
```

--

* Run a command containing a string, such as "stash" with `!?stash?`

```bash
git stash
git checkout master
!?sta pop
```

--

#### Accessing Arguments ([Word Designators](http://www.gnu.org/software/bash/manual/bashref.html#Word-Designators))

--

##### Colons - Ever Vital

Building on the above, colons (:)

Used to select a word in the command.

0 is the initial command, and subsequent words are selected by increasing the number.

```bash
$ ls ./
0.stashed  1.stashed  2.stashed  3.stashed  4.stashed  5.stashed  6.stashed  7.stashed  README.txt  test
$ !!:0
ls
0.stashed  1.stashed  2.stashed  3.stashed  4.stashed  5.stashed  6.stashed  7.stashed  README.txt  test
```

--

* A useful shortcut for accessing the first argument: `^`
* A useful shortcut for accessing the last argument: `$`

--

Example:

```bash
$ touch a.txt b.txt
$ rm !^
rm a.txt
$ rm !-2:$
rm b.txt
```

This isn't great if we want to access a range of arguments. Here's some ways to work with ranges:

```bash
# Create 3 files
$ touch a.txt b.txt c.txt d.txt e.txt
# Remove from the third file onwards.
$ rm !!:3*
rm c.txt d.txt e.txt
# Remove the first two arguments from two commands ago
$ rm !-2:1-2
rm a.txt b.txt
```

--

#### Modifiers

The final, powerful piece of the history expansion puzzle (Again, under the [Reference Document](http://www.gnu.org/software/bash/manual/bashref.html#Modifiers))

These are tacked onto the end of what we covered above, separated by colons (:).

The most common usage is global substitution.

```bash
$ echo "Some very foo sentence foo"
Some very foo sentence foo
$ !-1:gs/foo/bar/
echo "Some very bar sentence bar"
Some very bar sentence bar
```

---

## Some things which are just cool 😎

* [FZF](https://github.com/junegunn/fzf)
* [FPP](https://facebook.github.io/PathPicker/)
* [ZSH Prompts - E.g. Spaceship Prompt](https://github.com/denysdovhan/spaceship-prompt)
* [Git Number](https://github.com/holygeek/git-number)
* [RipGrep](https://github.com/BurntSushi/ripgrep)

---

## Tutorials & resources

* [Awesome Shell](https://github.com/alebcay/awesome-shell)
* [Awesome Bash](https://github.com/awesome-lists/awesome-bash)
* [Awesome Command Line Apps](https://github.com/herrbischoff/awesome-command-line-apps)

---
